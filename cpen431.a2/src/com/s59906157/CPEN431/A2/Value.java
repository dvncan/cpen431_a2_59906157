package com.s59906157.CPEN431.A2;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Value {

	public static byte[] getVal(ByteBuffer bb, int length) throws OutOfMemoryError{
		
		
		byte[] valueB = new byte[length];
		bb.position(51);
		bb.get(valueB);
		System.out.println("Value: " + Arrays.toString(valueB));
		
		return valueB;
	}
	
	public static byte[] getIdVal(ByteBuffer bb){
		
		byte[] valueId = new byte[16];
		bb.get(valueId);
		System.out.println("Value ID: " + Arrays.toString(valueId));
		
		return valueId;
	}
	
	public static byte[] valLength2bytes(int length){
		byte[] prt = new byte[2];
		// 4 digits -> 2bytes
		prt[0] = (byte) (length & 0xFF);
		prt[1] = (byte) ((length >> 8) & 0xFF);
		
		return prt;		
	}
	
	

}
