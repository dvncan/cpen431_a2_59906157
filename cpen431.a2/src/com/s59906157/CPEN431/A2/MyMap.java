package com.s59906157.CPEN431.A2;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

public class MyMap implements ConcurrentMap {

	private static final int MAX = 50000;
	HashMap<ByteBuffer, byte[]> map;
	Error erMsg = null;
	byte er;
	long myTotalMemoryBefore;
	long myTotalMemoryAfter;
	long myHashMapMemory;

	public MyMap(){
		map = new HashMap<ByteBuffer, byte[]>();
		myTotalMemoryBefore = Runtime.getRuntime().freeMemory();

	}

	public byte put(ByteBuffer keyBuffer, int length, byte[] value) throws ExceptionSystemOverload{

		if(map.put(keyBuffer, value) == null){
//			if(this.put(keyBuffer, value)){
				//put key & value in hash map.
				
				map.put(keyBuffer, value);

				myTotalMemoryAfter = Runtime.getRuntime().freeMemory();
				myHashMapMemory =  myTotalMemoryAfter - myTotalMemoryBefore ;
				
//				System.out.println("Memory: "+ Math.abs(myHashMapMemory) );
				
				if(Math.abs(myHashMapMemory/1000000) <= 44){
				
				System.out.println("Value was successfully set.");
				System.out.println(map.size() + "");
				er = (0x00);
				}else{
//					System.out.println("Here memoryError");
					throw new OutOfMemoryError();
				}
//			}else{
//				throw new ExceptionSystemOverload();
//			}
		}else{

			map.put(keyBuffer, value);
			System.out.println("Value was successfully set.");
			er = (0x00);

		}
		
		return er;
	}

	public byte check(ByteBuffer keyBuffer) {
		//Check to see if get will be successful.
		if(map.get(keyBuffer)==null){
			System.out.print("No value associated with this key. ");
			//			erMsg.setVal(1);
			er = (0x01);
		}else{
			//			erMsg.setVal(0);
			er = (0x00);
		}
		return er;	
	}

	public int size(){
		return map.size();
	}

	public byte[] get(ByteBuffer keyBuffer){
		byte[] replyValue;

		replyValue = map.get(keyBuffer);

		return replyValue;
	}

	public void remove(ByteBuffer keyBuffer) {
		map.remove(keyBuffer);
	}

	public void clear() {
		map.clear();
		er = (0x00);
	}

	public byte getEr(){
		return er;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsKey(Object key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsValue(Object value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object get(Object key) {
		// TODO Auto-generated method stub
		return null;
	}

	//http://stackoverflow.com/questions/5601333/limiting-the-max-size-of-a-hashmap-in-java 
	public boolean put(ByteBuffer key, byte[] value) {
		if (map.size() >= MAX && !map.containsKey(key)) {
			return false;
		} else {
			return true;
		}
	}
	

	@Override
	public Object remove(Object key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void putAll(Map m) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set keySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection values() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object putIfAbsent(Object key, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object key, Object value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean replace(Object key, Object oldValue, Object newValue) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object replace(Object key, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object put(Object key, Object value) {
		// TODO Auto-generated method stub
		return null;
	}
}
