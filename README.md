# READ ME #

Duncan Brown - 59906157

Port number - 16157

I tried to separate my application level as much as possible by creating classes for each task the server should run. My server class starts a ServerThread class which handles everything. I chose this to allow multiple clients to connect. My hash map is a ConcurrentHashMap to allow multiple threads to access it. My error handling is mostly handled by returning the correct byte representation of the error, except for OutOfMemoryError which is thrown and caught in the ServerThread class, where I assign the error variable the correct code. 

My server passes the tests provided, however, it is designed for more than given.